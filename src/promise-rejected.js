function rejectPromise() {

  // Implement this promise so it rejects with an Error containing the message "No Good!" after a 250ms delay"
  const promise = new Promise((resolve, reject) => {
    // Your code
  });

  // Don't forget to print the error message after fulfilling the promise using .catch

}

export default rejectPromise;
