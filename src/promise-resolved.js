function resolvePromise() {

  // Implement this promise so it resolves with the string "All Done!" after a 250ms delay"
  const promise = new Promise((resolve, reject) => {
    // Your code
  });

  // Don't forget to print the contents after fulfilling the promise using .then

}

export default resolvePromise;
